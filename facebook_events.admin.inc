<?php

/**
 * Settings form
 * @param array $form
 * @param array $form_state
 * @param array $reset
 */
function facebook_events_settings_form($form, $form_state, $reset = FALSE) {
  $settings = isset($form_state['input']['facebook_events_settings']) ? $form_state['input']['facebook_events_settings'] : variable_get('facebook_events_settings', array());
  $form['facebook_events_settings'] = array(
    '#tree' => TRUE,
  );

  $form['facebook_events_settings']['app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => isset($settings['app_id']) ? $settings['app_id'] : '',
    '#required' => TRUE,
  );

  $form['facebook_events_settings']['app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#default_value' => isset($settings['app_secret']) ? $settings['app_secret'] : '',
    '#required' => TRUE,
  );

  if (isset($settings['app_id']) && isset($settings['app_secret']) && $facebook = facebook_events_facebook_sdk()) {
    // Authenticate Facebook account.
    if ($reset || empty($settings) || !isset($settings['access_token']) || (isset($settings['access_token']) && !facebook_events_facebook_sdk_is_authenticated($facebook, $settings['access_token']))) {
      if (isset($_GET['code'])) {
        $settings['access_token'] = $facebook->getAccessToken();
        $settings['access_expiry'] = 0;

        variable_set('facebook_events_settings', $settings);
        drupal_goto('admin/config/services/fbeventync');
      } else {
        unset($settings['access_token']);
        unset($settings['access_expiry']);
        variable_set('facebook_events_settings', $settings);
        if (!isset($_SESSION[md5(serialize($settings))])) {
          $_SESSION[md5(serialize($settings))] = FALSE;
          drupal_goto($facebook->getLoginUrl(array('scope' => array('manage_pages', 'offline_access', 'publish_stream', 'user_photos'))));
        } else {
          unset($_SESSION[md5(serialize($settings))]);
          form_set_error('facebook_events_settings', 'Authentication failed');
          return system_settings_form($form);
        }
      }
    }
    if (!empty($settings)) {
      $form['facebook_events_settings']['access_token'] = array(
        '#type' => 'value',
        '#value' => $settings['access_token'],
      );
      $form['facebook_events_settings']['authenticated'] = array(
        '#type' => 'fieldset',
        '#title' => t('Facebook user'),
        '#description' => theme('facebook_events_authenticated_user', array(
          'account' => facebook_events_facebook_sdk_get($facebook, '/me'),
          'access_token' => $settings['access_token'],
          'access_expiry' => $settings['access_expiry'],
        )),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Account config form
 * @param type $form
 * @param type $form_state
 * @return type
 */
function facebook_events_account_config_form($form, $form_state) {
  $form = array();
  $settings = isset($form_state['input']['facebook_events_account']) ? $form_state['input']['facebook_events_account'] : variable_get('facebook_events_account', array());
  $facebook = facebook_events_facebook_sdk();

  $form['facebook_events_account'] = array(
    '#tree' => TRUE,
  );
  $form['facebook_events_account']['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account settings'),
    '#description' => t('Select account to sync event'),
  );
  $form['facebook_events_account']['account']['id'] = array(
    '#type' => 'select',
    '#title' => t('Account'),
    '#options' => facebook_events_account_ids($facebook),
    '#default_value' => isset($settings['account']['id']) ? $settings['account']['id'] : key(facebook_events_account_ids($facebook)),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
